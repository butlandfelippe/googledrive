export default {
  clearMocks: true,
  restoreMocks: true,
  collectCoverage: true,
  coverageDirectory: "coverage",
  coverageProvider: "v8",
  coveregeReporters: [
    "text", 
    "lcov"
  ],
  testEnvironment: "node",
  coverageThreshold: {
    global: {
      branches: 50,
      functions: 50,
      lines: 50,
      statements: 50
    }
  },
  watchPathIgnorePatterns: [ 
    "node_modules",
  ],
  transformIgnorePatterns: [
    "node_modules",
  ],
  collectCoverageFrom: [ 
    "src/**/*.js", "!src/**/index.js"
  ] 
};
